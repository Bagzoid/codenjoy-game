package com.codenjoy.dojo.lines.model;

import com.codenjoy.dojo.lines.services.*;
import com.codenjoy.dojo.services.*;

import java.util.*;

/**
 * О! Это самое сердце игры - борда, на которой все происходит.
 * Если какой-то из жителей борды вдруг захочет узнать что-то у нее, то лучше ему дать интефейс {@see Field}
 * Борда реализует интерфейс {@see Tickable} чтобы быть уведомленной о каждом тике игры. Обрати внимание на {Sample#tick()}
 */
public class Lines implements Tickable, Field {

    public static final int NEW_BALLS_COUNT = 3;
    private final int size;
    private final Dice dice;
    private final TemplateDice<Elements> ballDice;
    private List<Ball> balls;
    private Player player;
    private LengthToXY lengthToXY;

    public Lines(Level level, Dice dice, TemplateDice<Elements> ballDice) {
        this.dice = dice;
        this.ballDice = ballDice;
        balls = level.getBalls();
        size = level.getSize();
        lengthToXY = new LengthToXY(size);
        addRandomBalls(NEW_BALLS_COUNT);
    }

    /**
     * @see Tickable#tick()
     */
    @Override
    public void tick() {
        if (player != null) {
            processPlayerMove();
            if (!collapseBalls()) {
                addRandomBalls(NEW_BALLS_COUNT);
                collapseBalls();
            }
            checkGameOver();
        }
    }

    private void addRandomBalls(int count) {
        for (int i = 0; i < count; i++) {
            addRandomBall();
        }
    }

    private void addRandomBall() {
        Point freeRandom = getFreeRandom();
        setBall(freeRandom.getX(), freeRandom.getY(), ballDice.next());
    }

    /**
     * Collapse balls
     *
     * @return true if board changed
     */
    private boolean collapseBalls() {
        Set<Ball> ballSet = new HashSet<>();
        for (Ball ball : getBalls()) {
            for (Directions direction : Directions.values()) {
                ballSet.addAll(checkCollapse(ball, direction));
            }
        }
        if (ballSet.size() > 0) {
            for (int i = 0; i < ballSet.size(); i++) {
                player.event(Events.WIN);
            }
            return balls.removeAll(ballSet);
        }
        return false;
    }

    private Set<Ball> checkCollapse(Ball ballToCheck, Directions direction) {
        Set<Ball> ballsResult;
        ballsResult = new HashSet<>();
        ballsResult.add(ballToCheck);
        for (PointImpl directionPoint : direction.getDirections()) {
            for (int x = ballToCheck.getX(), y = ballToCheck.getY(); !(new PointImpl(x, y).isOutOf(size)); x += directionPoint.getX(), y += directionPoint.getY()) {
                Ball ball = getBall(x, y);
                if (ball == null || !ball.getBallType().equals(ballToCheck.getBallType())) {
                    break;
                }
                ballsResult.add(ball);
            }
        }
        if (ballsResult.size() < 5) ballsResult.clear();
        return ballsResult;
    }

    private void checkGameOver() {
        if (isGameOver()) {
            player.event(Events.LOOSE);
        }
    }

    private void processPlayerMove() {
        player.tick();
    }

    public int size() {
        return size;
    }

    @Override
    public Point getFreeRandom() {
        int rndX = 0;
        int rndY = 0;
        int c = 0;
        do {
            rndX = dice.next(size);
            rndY = dice.next(size);
        } while (!isFree(rndX, rndY) && c++ < 100);

        if (c >= 100) {
            return PointImpl.pt(0, 0);
        }

        return PointImpl.pt(rndX, rndY);
    }

    @Override
    public boolean isFree(int x, int y) {
        Point pt = PointImpl.pt(x, y);

        return !balls.contains(pt);
    }

    @Override
    public boolean isBall(int x, int y) {
        return balls.contains(PointImpl.pt(x, y));
    }

    @Override
    public Ball getBall(int x, int y) {
        Point point = new PointImpl(x, y);
        for (Ball ball : balls) {
            if (ball.equals(point)) {
                return ball;
            }
        }
        return null;
    }

    @Override
    public void setBall(int x, int y, Elements ballType) {
        if (ballType == null) {
            return;
        }
        Point pt = PointImpl.pt(x, y);
        if (!balls.contains(pt)) {
            balls.add(new Ball(x, y, ballType));
        }
    }

    @Override
    public void removeBall(int x, int y) {
        balls.remove(PointImpl.pt(x, y));
    }

    @Override
    public void moveBall(Point fromPoint, Point toPoint) {
        int i = balls.indexOf(fromPoint);
        if (i >= 0 && isValid(toPoint) && isFree(toPoint.getX(), toPoint.getY())) {
            if (new FindWay(size).isWayExist(ballsToPoints(), fromPoint, toPoint)) {
                Ball ball = balls.get(i);
                if (balls.remove(ball)) {
                    balls.add(new Ball(toPoint, ball.getBallType()));
                }
            }
        }
    }

    public void newGame(Player player) {
        if (this.player == null) {
            this.player = player;
        }
    }

    public void remove() {
        player = null;
    }

    public List<Ball> getBalls() {
        return balls;
    }


    public BoardReader reader() {
        return new BoardReader() {
            private int size = Lines.this.size;

            @Override
            public int size() {
                return size;
            }

            @Override
            public Iterable<? extends Point> elements() {
                List<Point> result = new LinkedList<Point>();
                result.addAll(Lines.this.getBalls());
                return result;
            }
        };
    }

    public boolean isGameOver() {
        return balls.size() >= size;
    }


    private boolean isValid(Point point) {
        if (point == null) {
            return false;
        }
        return !point.isOutOf(size);
    }

    private List<Point> ballsToPoints() {
        List<Point> points = new LinkedList<>();
        for (Ball ball : balls) {
            points.add(new PointImpl(ball.getX(), ball.getY()));
        }
        return points;
    }

}
