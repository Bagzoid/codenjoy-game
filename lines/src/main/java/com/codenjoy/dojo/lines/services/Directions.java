package com.codenjoy.dojo.lines.services;

import com.codenjoy.dojo.services.PointImpl;

import java.util.Arrays;
import java.util.List;

public enum Directions {
    HORIZONTAL(new PointImpl(-1, 0), new PointImpl(1, 0)),
    VERTICAL(new PointImpl(0, -1), new PointImpl(0, 1)),
    DIAGONAL1(new PointImpl(-1, -1), new PointImpl(1, 1)),
    DIAGONAL2(new PointImpl(1, -1), new PointImpl(-1, 1));

    private List<PointImpl> direction;

    Directions(PointImpl ... points) {
        direction = Arrays.asList(points);
    }

    public List<PointImpl> getDirections() {
        return direction;
    }
}
