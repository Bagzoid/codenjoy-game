package com.codenjoy.dojo.lines.services;

import com.codenjoy.dojo.services.Joystick;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;

/**
 * Created by akudl on 2015-11-29.
 */
public class LastActMoveJoystick implements Joystick {
    private Point fromPoint = null;
    private Point toPoint = null;

    @Override
    public void down() {

    }

    @Override
    public void up() {

    }

    @Override
    public void left() {

    }

    @Override
    public void right() {

    }

    @Override
    public void act(int... p) {
        fromPoint = null;
        toPoint = null;

        if (p.length == 4) {
            fromPoint = new PointImpl(p[0], p[1]);
            toPoint = new PointImpl(p[2], p[3]);
        }

    }

    public Point getFromPoint() {
        return fromPoint;
    }

    public Point getToPoint() {
        return toPoint;
    }
}
