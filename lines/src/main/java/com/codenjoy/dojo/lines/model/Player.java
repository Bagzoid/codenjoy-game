package com.codenjoy.dojo.lines.model;

import com.codenjoy.dojo.lines.services.Events;
import com.codenjoy.dojo.lines.services.LastActMoveJoystick;
import com.codenjoy.dojo.services.EventListener;
import com.codenjoy.dojo.services.Joystick;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;

/**
 * Класс игрока. Тут кроме героя может подсчитываться очки. Тут же ивенты передаются лиснеру фреймворка.
 */
public class Player {

    private final EventListener listener;
    private final Field field;
    private final LastActMoveJoystick joystick;
    private int maxScore;
    private int score;

    /**
     * @param listener Это шпийон от фреймоврка. Ты должен все ивенты которые касаются конкретного пользователя сормить ему.
     * @param field
     */
    public Player(EventListener listener, final Field field) {
        this.listener = listener;
        this.field = field;
        clearScore();
        joystick = new LastActMoveJoystick();
    }

    private void increaseScore() {
        score = score + 1;
        maxScore = Math.max(maxScore, score);
    }

    public int getMaxScore() {
        return maxScore;
    }

    public int getScore() {
        return score;
    }

    /**
     * Борда может файрить ивенты юзера с помощью этого метода
     *
     * @param event тип ивента
     */
    public void event(Events event) {
        switch (event) {
            case LOOSE:
                gameOver();
                break;
            case WIN:
                increaseScore();
                break;
        }

        if (listener != null) {
            listener.event(event);
        }
    }

    private void gameOver() {
        score = 0;
    }

    public void clearScore() {
        score = 0;
        maxScore = 0;
    }

    public Joystick getJoystick() {
        return joystick;
    }

    public void tick() {
        Point fromPoint = joystick.getFromPoint();
        Point toPoint = joystick.getToPoint();
        unsetMove();

        if (fromPoint != null && toPoint != null) {
            field.moveBall(fromPoint, toPoint);
        }
    }

    private void unsetMove() {
        joystick.act(0);
    }
}