package com.codenjoy.dojo.lines.model;

import com.codenjoy.dojo.services.Point;

/**
 * Так случилось что у меня доска знает про героя, а герой про доску. И чтобы герой не знал про всю доску, я ему даю вот эту часть доски.
 */
public interface Field {

    Point getFreeRandom();

    boolean isFree(int x, int y);

    boolean isBall(int x, int y);

    Ball getBall(int x, int y);

    void setBall(int x, int y, Elements ballType);

    void removeBall(int x, int y);

    void moveBall(Point fromPoint, Point toPoint);
}
