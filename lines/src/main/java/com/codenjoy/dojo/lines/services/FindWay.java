package com.codenjoy.dojo.lines.services;

import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;

import java.util.List;

/**
 * Created by akudl on 2015-11-29.
 */
public class FindWay {
    private int board[][];
    private int size;

    public FindWay(int size) {
        board = new int[size][size];
        this.size = size;
    }

    public boolean isWayExist(List<Point> points, Point from, Point destination) {
        board = new int[size][size];

        for (Point point : points) {
            if (point.equals(from)) {
                continue;
            }
            board[point.getX()][point.getY()] = -1;
        }
        return checkWays(destination, from);
    }

    private boolean checkWays(Point destination, Point from) {
        if (from.equals(destination)) return true;
        PointImpl point = new PointImpl(from.getX() + 1, from.getY());
        if (checkWayNext(destination, point)) return true;
        point = new PointImpl(from.getX() - 1, from.getY());
        if (checkWayNext(destination, point)) return true;
        point = new PointImpl(from.getX(), from.getY() - 1);
        if (checkWayNext(destination, point)) return true;
        point = new PointImpl(from.getX(), from.getY() + 1);
        if (checkWayNext(destination, point)) return true;
        return false;
    }

    private boolean checkWayNext(Point destination, PointImpl point) {
        if (!point.isOutOf(size)) {
            if (board[point.getX()][point.getY()] == 0) {
                if (point.equals(destination)) return true;
                board[point.getX()][point.getY()] = 1;
                if (checkWays(destination, point)) return true;
            }
        }
        return false;
    }

    ;

}
