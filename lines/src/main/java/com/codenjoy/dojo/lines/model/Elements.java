package com.codenjoy.dojo.lines.model;

import com.codenjoy.dojo.services.CharElements;

import java.util.Random;

/**
 * Тут указана легенда всех возможных объектов на поле и их состояний.
 * Важно помнить, что для каждой енумной константы надо создать спрайт в папке \src\main\webapp\resources\sprite.
 */
public enum Elements implements CharElements {

    NONE(' '),
    RED('R'),
    GREEN('G'),
    PINK('P'),
    BROWN('B'),
    BLUE('L'),
    YELLOW('Y'),
    SKY('S');

    final char ch;

    Elements(char ch) {
        this.ch = ch;
    }

    @Override
    public char ch() {
        return ch;
    }

    @Override
    public String toString() {
        return String.valueOf(ch);
    }

    public static Elements valueOf(char ch) {
        for (Elements el : Elements.values()) {
            if (el.ch == ch) {
                return el;
            }
        }
        throw new IllegalArgumentException("No such element for " + ch);
    }

    public static Elements randomBall() {
        StringBuilder stringBuilder = new StringBuilder(Elements.values().length);
        for (Elements el : Elements.values()) {
            if (isBall(el)) {
                stringBuilder.append(el.ch());
            }
        }
        Random random = new Random();
        int maxBalls = stringBuilder.length() - 1;
        int i = random.nextInt(maxBalls);
        return valueOf(stringBuilder.charAt(i));
    }

    public static boolean isBall(Elements ballType) {
        return ballType != null && ballType != NONE;
    }
}
