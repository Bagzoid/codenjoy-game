package com.codenjoy.dojo.lines.model;

import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;
import com.codenjoy.dojo.services.State;

/**
 * Artifact Ball
 */
public class Ball extends PointImpl implements State<Elements, Player> {
    private Elements ballType;

    public Ball(int x, int y, Elements ballType) {
        super(x, y);
        validateBallType(ballType);
        this.ballType = ballType;
    }

    /**
     * Create random ball type
     * @param x
     * @param y
     */
    public Ball(int x, int y) {
        super(x, y);
        ballType = Elements.randomBall();
        validateBallType(ballType);
    }

    private void validateBallType(Elements ballType) {
        if (! Elements.isBall(ballType)) {
            throw new RuntimeException("Unsupported ball type: " + ballType);
        }
    }

    public Ball(Point point, Elements ballType) {
        super(point);
        validateBallType(ballType);
        this.ballType = ballType;
    }

    @Override
    public Elements state(Player player, Object... alsoAtPoint) {
        return ballType;
    }

    public Elements getBallType() {
        return ballType;
    }
}
