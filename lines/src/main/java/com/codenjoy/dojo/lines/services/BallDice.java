package com.codenjoy.dojo.lines.services;

import com.codenjoy.dojo.lines.model.Elements;

public class BallDice implements TemplateDice<Elements> {
    @Override
    public Elements next() {
        return Elements.randomBall();
    }
}
