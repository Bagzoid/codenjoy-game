package com.codenjoy.dojo.lines.services;

public interface TemplateDice<T> {
    T next();
}
