package com.codenjoy.dojo.lines.client;

import com.codenjoy.dojo.client.AbstractBoard;
import com.codenjoy.dojo.lines.model.Elements;
import com.codenjoy.dojo.lines.model.Elements;
import com.codenjoy.dojo.services.Point;

/**
 * Класс, обрабатывающий строковое представление доски.
 * Содержит ряд унаследованных методов {@see AbstractBoard},
 * но ты можешь добавить сюда любые свои методы на их основе.
 */
public class Board extends AbstractBoard<Elements> {

    @Override
    public Elements valueOf(char ch) {
        return Elements.valueOf(ch);
    }

    public boolean isGameOver() {
        return get(Elements.NONE).isEmpty();
    }

    public boolean isBarrierAt(int x, int y) {
        return !isAt(x, y, Elements.NONE);
    }
}