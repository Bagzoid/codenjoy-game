package com.codenjoy.dojo.lines.model;

import com.codenjoy.dojo.lines.services.BallDice;
import com.codenjoy.dojo.services.*;
import com.codenjoy.dojo.utils.TestUtils;
import com.codenjoy.dojo.lines.services.Events;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * User: sanja
 * Date: 17.12.13
 * Time: 4:47
 */
public class LinesTest {

    private Lines game;
    private Dice dice;
    private BallDice ballDice;
    private EventListener listener;
    private Player player;
    private Joystick joystick;
    private PrinterFactory printer = new PrinterFactoryImpl();

    @Before
    public void setup() {
        dice = mock(Dice.class);
        ballDice = mock(BallDice.class);
    }

    private void dice(int... ints) {
        OngoingStubbing<Integer> when = when(dice.next(anyInt()));
        for (int i : ints) {
            when = when.thenReturn(i);
        }
    }

    private void ballDice(Elements... vals) {
        OngoingStubbing<Elements> when = when(ballDice.next());
        for (Elements val : vals) {
            when = when.thenReturn(val);
        }
    }

    private void givenFl(String board) {
        LevelImpl level = new LevelImpl(board);

        game = new Lines(level, dice, ballDice);
        listener = mock(EventListener.class);
        player = new Player(listener, game);
        joystick = player.getJoystick();
        game.newGame(player);
    }

    private void assertE(String expected) {
        assertEquals(TestUtils.injectN(expected),
                printer.getPrinter(game.reader(), player).print());
    }

    @Test
    public void shouldFieldAtStart() {
        givenFl("     " +
                "     " +
                "  B  " +
                "     " +
                "     ");

        assertE("     " +
                "     " +
                "  B  " +
                "     " +
                "     ");
    }

    @Test
    public void shouldWalk() {
        givenFl("     " +
                "     " +
                "  R  " +
                "     " +
                "     ");

        joystick.act(2, 2, 1, 1);
        game.tick();

        assertE("     " +
                "     " +
                "     " +
                " R   " +
                "     ");


        joystick.act(1, 1, 4, 4);
        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
        joystick.act(4, 4, 5, 5);
        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldStopWhenNoField() {
        givenFl("    R" +
                "     " +
                "     " +
                "     " +
                "     ");

        joystick.act(4, 4, 5, 5);
        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");

        joystick.act(4, 4, -1, -1);
        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldStopWhenNoCommand() {
        givenFl("    R" +
                "     " +
                "     " +
                "     " +
                "     ");

        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldStopWhenNoBall() {
        givenFl("    R" +
                "     " +
                "     " +
                "     " +
                "     ");

        joystick.act(3, 3, 2, 2);
        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    //TODO
    public void shouldStopWhenOverlap() {
        givenFl("    R" +
                "     " +
                "     " +
                "     " +
                "B    ");

        joystick.act(0, 0, 4, 4);
        game.tick();

        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "B    ");
    }

    @Test
    public void shouldStopWhenNoWay() {
        givenFl("     " +
                "  R  " +
                " R R " +
                "  R  " +
                "G    ");

        joystick.act(0, 0, 2, 2);
        game.tick();

        assertE("     " +
                "  R  " +
                " R R " +
                "  R  " +
                "G    ");
    }


    @Test
    public void shouldLoose() {
        givenFl("RGBYP" +
                "SRGBY" +
                "YSBYP" +
                "BYSRG" +
                "RGBYP");

        game.tick();
        verify(listener).event(Events.LOOSE);
        assertE("RGBYP" +
                "SRGBY" +
                "YSBYP" +
                "BYSRG" +
                "RGBYP");
    }

    @Test
    public void shouldColapse() {
        givenFl("     " +
                "     " +
                "     " +
                "     " +
                "BBBBB");

        game.tick();
        verify(listener, times(5)).event(Events.WIN);
        assertE("     " +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldNotColapse() {
        givenFl("    B" +
                "    B" +
                "    B" +
                "    B" +
                "BBBB ");

        game.tick();
        assertE("    B" +
                "    B" +
                "    B" +
                "    B" +
                "BBBB ");
    }

    @Test
    public void shouldColapseTwoLine() {
        givenFl("  B R" +
                "  B  " +
                "  B  " +
                "  B  " +
                "BBBBB");

        game.tick();

        verify(listener, times(9)).event(Events.WIN);
        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldAddBalls() {
        givenFl("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
        dice(0, 0, 1, 1, 2, 2);
        ballDice(Elements.RED, Elements.GREEN, Elements.BLUE);
        game.tick();
        assertE("    R" +
                "     " +
                "  L  " +
                " G   " +
                "R    ");
    }

    @Test
    public void shouldNotAddBalls() {
        givenFl("G   R" +
                "G    " +
                "G    " +
                "G    " +
                "G    ");
        dice(0, 0, 1, 1, 2, 2);
        ballDice(Elements.RED, Elements.GREEN, Elements.BLUE);
        game.tick();
        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldCollapseAddedBalls() {
        givenFl("RGL R" +
                "RGL  " +
                "RG   " +
                "R L  " +
                " GL  ");
        dice(0, 0, 1, 1, 2, 2);
        ballDice(Elements.RED, Elements.GREEN, Elements.BLUE);
        game.tick();
        verify(listener, times(15)).event(Events.WIN);
        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

    @Test
    public void shouldMoveAndCollapseAddedBalls() {
        givenFl("RGL R" +
                "RGL  " +
                "RG   " +
                "R L  " +
                " G L ");
        dice(0, 0, 1, 1, 2, 2);
        ballDice(Elements.RED, Elements.GREEN, Elements.BLUE);
        joystick.act(3, 0, 2, 0);
        game.tick();
        verify(listener, times(15)).event(Events.WIN);
        assertE("    R" +
                "     " +
                "     " +
                "     " +
                "     ");
    }

}
